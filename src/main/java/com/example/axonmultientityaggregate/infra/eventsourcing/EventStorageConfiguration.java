package com.example.axonmultientityaggregate.infra.eventsourcing;

import com.mongodb.client.MongoClient;
import org.axonframework.eventhandling.tokenstore.TokenStore;
import org.axonframework.eventsourcing.eventstore.EmbeddedEventStore;
import org.axonframework.eventsourcing.eventstore.EventStorageEngine;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.axonframework.extensions.mongo.DefaultMongoTemplate;
import org.axonframework.extensions.mongo.MongoTemplate;
import org.axonframework.extensions.mongo.eventsourcing.eventstore.MongoEventStorageEngine;
import org.axonframework.extensions.mongo.eventsourcing.tokenstore.MongoTokenStore;
import org.axonframework.serialization.Serializer;
import org.axonframework.spring.config.AxonConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EventStorageConfiguration {

  @Bean
  public EmbeddedEventStore eventStore(EventStorageEngine storageEngine, AxonConfiguration configuration) {
    return EmbeddedEventStore.builder()
                             .storageEngine(storageEngine)
                             .messageMonitor(configuration.messageMonitor(EventStore.class, "eventStore"))
                             .build();
  }

  @Bean
  public EventStorageEngine storageEngine(MongoTemplate axonTemplate, Serializer jackson) {
    return MongoEventStorageEngine.builder()
                                  .mongoTemplate(axonTemplate)
                                  .eventSerializer(jackson)
                                  .snapshotSerializer(jackson)
                                  .build();
  }

  @Bean
  MongoTemplate axonTemplate(MongoClient client, MongoProperties props) {
    return DefaultMongoTemplate.builder()
                               .mongoDatabase(client, props.getDatabase())
                               .build();
  }

  @Bean
  public TokenStore axonTokenStore(MongoTemplate axonTemplate, Serializer jackson) {
    return MongoTokenStore.builder()
                          .mongoTemplate(axonTemplate)
                          .serializer(jackson)
                          .build();
  }
}
