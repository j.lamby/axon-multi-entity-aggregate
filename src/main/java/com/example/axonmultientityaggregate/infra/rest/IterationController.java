package com.example.axonmultientityaggregate.infra.rest;

import com.example.axonmultientityaggregate.domain.iteration.Iteration;
import com.example.axonmultientityaggregate.domain.iteration.StartIterationCommand;
import com.example.axonmultientityaggregate.domain.step.StartStepCommand;
import com.example.axonmultientityaggregate.domain.step.StepStatus;
import com.example.axonmultientityaggregate.domain.step.StepType;
import com.example.axonmultientityaggregate.domain.substep.EndSubStepWithStatusCommand;
import com.example.axonmultientityaggregate.domain.substep.SubStepStatus;
import com.example.axonmultientityaggregate.domain.substep.SubStepType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.extensions.reactor.commandhandling.gateway.ReactorCommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.List;

@Slf4j
@Controller
@RequiredArgsConstructor
public class IterationController {

    public static final Iteration.IterationKey ITERATION_KEY = new Iteration.IterationKey(LocalDate.now(), "01", 0);

    @Autowired
    private ReactorCommandGateway commandGateway;

    public Mono<ServerResponse> createIteration(ServerRequest serverRequest) {

        var commands = List.of(
            new StartIterationCommand(ITERATION_KEY)
        );

        return Flux.fromIterable(commands)
                .flatMap(commandGateway::send)
                .then(ServerResponse.ok().build());
    }

    public Mono<ServerResponse> createStep(ServerRequest serverRequest) {
        var commands = List.of(
                new StartStepCommand(ITERATION_KEY, StepType.ACCEPTABILITY, StepStatus.COMPLETED),
                new StartStepCommand(ITERATION_KEY, StepType.DELETE_NOMINATIONS, StepStatus.IN_PROGRESS)
        );

        return Flux.fromIterable(commands)
                .flatMap(commandGateway::send)
                .then(ServerResponse.ok().build());
    }

    public Mono<ServerResponse> createSubstep(ServerRequest serverRequest) {
        var commands = List.of(
                new EndSubStepWithStatusCommand(ITERATION_KEY, StepType.ACCEPTABILITY, SubStepType.NOMINATION_PROCESSED, SubStepStatus.KO),
                new EndSubStepWithStatusCommand(ITERATION_KEY, StepType.DELETE_NOMINATIONS, SubStepType.REMOTE_SERVICE_CALL, SubStepStatus.KO)
        );

        return Flux.fromIterable(commands)
                .flatMap(commandGateway::send)
                .then(ServerResponse.ok().build());
    }
}
