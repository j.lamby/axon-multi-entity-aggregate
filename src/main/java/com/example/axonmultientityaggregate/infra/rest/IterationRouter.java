package com.example.axonmultientityaggregate.infra.rest;

import com.example.axonmultientityaggregate.infra.config.web.UrlConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class IterationRouter {

  private static final String COLLECTION_URL = UrlConstants.BASE_URL + "/iterations";
  private static final String LOL1 = COLLECTION_URL + "/lol1";
  private static final String LOL2 = COLLECTION_URL + "/lol2";
  private static final String LOL3 = COLLECTION_URL + "/lol3";

  @Bean
  RouterFunction<ServerResponse> processGasDayIterationsRoutes(IterationController controller) {

    return route()
        .GET(LOL1, controller::createIteration)
        .GET(LOL2, controller::createStep)
        .GET(LOL3, controller::createSubstep)
        .build();
  }
}
