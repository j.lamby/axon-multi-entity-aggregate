package com.example.axonmultientityaggregate.infra.database.iteration;

import com.example.axonmultientityaggregate.domain.iteration.Iteration;
import com.example.axonmultientityaggregate.domain.iteration.IterationReadModel;
import com.example.axonmultientityaggregate.domain.iteration.IterationStatus;
import com.example.axonmultientityaggregate.domain.step.Step;
import com.example.axonmultientityaggregate.domain.step.StepType;
import lombok.Builder;
import lombok.Data;
import lombok.With;
import lombok.experimental.FieldNameConstants;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.Map;

@Data
@Builder
@FieldNameConstants
@Document("iteration")
public class IterationDocument {

    @Id
    private ObjectId id;
    private LocalDate gasDay;
    private String pgd;
    private Integer iteration;

    @With
    private IterationStatus status;

    @With
    private Map<StepType, Step> steps;

    public static IterationReadModel toReadModel(IterationDocument document) {
        var key = new Iteration.IterationKey(document.gasDay, document.pgd, document.iteration);

        return IterationReadModel.builder()
                .key(key)
                .status(document.getStatus())
                .steps(document.steps)
                .build();
    }

    public static IterationDocument toDocument(IterationReadModel readModel) {
        var key = readModel.getKey();

        return IterationDocument.builder()
                .id(ObjectId.get())
                .gasDay(key.gasDay())
                .pgd(key.pgd())
                .iteration(key.iteration())
                .steps(readModel.getSteps())
                .build();
    }
}
