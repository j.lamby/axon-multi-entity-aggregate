package com.example.axonmultientityaggregate.infra.database.iteration;

import com.example.axonmultientityaggregate.domain.iteration.IterationReadModel;
import com.example.axonmultientityaggregate.domain.iteration.IterationRepository;
import com.example.axonmultientityaggregate.domain.iteration.IterationStartedEvent;
import com.example.axonmultientityaggregate.domain.iteration.IterationStepAddedEvent;
import com.example.axonmultientityaggregate.domain.substep.SubStepEndedWithStatusEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Slf4j
@Service
@RequiredArgsConstructor
public class IterationProjection {

    private final IterationRepository repository;

    @EventHandler
    public void on(IterationStartedEvent event) {
        repository.insert(createReadModel(event))
                .doOnError(err -> log.error("Error during creation of Iteration : {}", event.getKey(), err))
                .block();
    }

    @EventHandler
    public void on(IterationStepAddedEvent event) {
        repository.findByKey(event.key())
                .map(model -> updateModel(model, event))
                .flatMap(repository::update)
                .doOnError(err -> log.error("Error during creation of Iteration : {}", event.key(), err))
                .block();
    }

    @EventHandler
    public void on(SubStepEndedWithStatusEvent event) {
        repository.findByKey(event.key())
                .map(model -> updateModel(model, event))
                .flatMap(repository::update)
                .doOnError(err -> log.error("Error during creation of Iteration : {}", event.key(), err))
                .block();
    }


    private IterationReadModel createReadModel(IterationStartedEvent event) {
        return IterationReadModel.builder()
                .key(event.getKey())
                .status(event.getStatus())
                .steps(new HashMap<>())
                .build();
    }

    private IterationReadModel updateModel(IterationReadModel model, IterationStepAddedEvent event) {
        model.getSteps().put(event.step().getType(), event.step());

        return IterationReadModel.builder()
                .steps(model.getSteps())
                .key(model.getKey())
                .status(model.getStatus())
                .build();
    }

    private IterationReadModel updateModel(IterationReadModel model, SubStepEndedWithStatusEvent event) {
        model.getSteps().get(event.type()).getSubsteps().get(event.subStepType()).setStatus(event.status());


        return IterationReadModel.builder()
                .steps(model.getSteps())
                .key(model.getKey())
                .status(model.getStatus())
                .build();
    }

}
