package com.example.axonmultientityaggregate.infra.database.iteration;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

public interface IterationMongoRepository extends ReactiveMongoRepository<IterationDocument, ObjectId> {
    Mono<IterationDocument> findByGasDayAndPgdAndIteration(LocalDate gasDay, String pgd, Integer iteration);
}
