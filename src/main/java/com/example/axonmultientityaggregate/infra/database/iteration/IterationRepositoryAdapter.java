package com.example.axonmultientityaggregate.infra.database.iteration;

import com.example.axonmultientityaggregate.domain.iteration.Iteration;
import com.example.axonmultientityaggregate.domain.iteration.IterationReadModel;
import com.example.axonmultientityaggregate.domain.iteration.IterationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
@RequiredArgsConstructor
public class IterationRepositoryAdapter implements IterationRepository {

    private final IterationMongoRepository repo;
    private final ReactiveMongoTemplate template;

    @Override
    public Mono<IterationReadModel> findByKey(Iteration.IterationKey key) {
        return repo.findByGasDayAndPgdAndIteration(key.gasDay(), key.pgd(), key.iteration())
                .map(IterationDocument::toReadModel);
    }

    @Override
    public Mono<IterationReadModel> insert(IterationReadModel readModel) {
        return repo.insert(IterationDocument.toDocument(readModel))
                .map(IterationDocument::toReadModel);
    }

    @Override
    public Mono<IterationReadModel> update(IterationReadModel readModel) {
        var key = readModel.getKey();

        return repo.findByGasDayAndPgdAndIteration(key.gasDay(), key.pgd(), key.iteration())
                .map(doc -> doc.withStatus(readModel.getStatus())
                        .withSteps(readModel.getSteps()))
                .flatMap(repo::save)
                .map(IterationDocument::toReadModel);
    }
}
