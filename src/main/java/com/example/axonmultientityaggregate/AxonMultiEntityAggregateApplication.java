package com.example.axonmultientityaggregate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AxonMultiEntityAggregateApplication {

    public static void main(String[] args) {
        SpringApplication.run(AxonMultiEntityAggregateApplication.class, args);
    }

}
