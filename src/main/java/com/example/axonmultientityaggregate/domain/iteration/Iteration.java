package com.example.axonmultientityaggregate.domain.iteration;

import com.example.axonmultientityaggregate.domain.step.StartStepCommand;
import com.example.axonmultientityaggregate.domain.step.Step;
import com.example.axonmultientityaggregate.domain.step.StepType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateMember;
import org.axonframework.spring.stereotype.Aggregate;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Slf4j
@Getter
@Aggregate
@NoArgsConstructor
@AllArgsConstructor
public class Iteration {

    public record IterationKey(LocalDate gasDay, String pgd, int iteration) {
    }

    @AggregateIdentifier
    private IterationKey key;
    private IterationStatus status;

    @AggregateMember
    private Map<StepType, Step> steps = new HashMap<>();


    @CommandHandler
    public Iteration(StartIterationCommand command) {
        log.info("Starting iteration {}", command);
        apply(IterationStartedEvent.builder()
                .key(command.getIterationKey())
                .status(IterationStatus.IN_PROGRESS)
                .build());
    }

    @CommandHandler
    public void handle(StartStepCommand command) {
        log.info("Starting start step in iteration {}", command);

        apply(new IterationStepAddedEvent(command.key(), new Step(command.type(), command.status())));
    }

    @EventSourcingHandler
    public void on(IterationStartedEvent event) {
        this.key = event.getKey();
        this.status = event.getStatus();
    }

    @EventSourcingHandler
    public void on(IterationStepAddedEvent event) {
        this.steps.put(event.step().getType(), event.step());
    }
}
