package com.example.axonmultientityaggregate.domain.iteration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class IterationStartedEvent {

  Iteration.IterationKey key;
  IterationStatus status;

}
