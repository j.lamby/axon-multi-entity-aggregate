package com.example.axonmultientityaggregate.domain.iteration;

import reactor.core.publisher.Mono;

public interface IterationRepository {
    Mono<IterationReadModel> findByKey(Iteration.IterationKey key);

    Mono<IterationReadModel> insert(IterationReadModel readModel);

    Mono<IterationReadModel> update(IterationReadModel readModel);
}
