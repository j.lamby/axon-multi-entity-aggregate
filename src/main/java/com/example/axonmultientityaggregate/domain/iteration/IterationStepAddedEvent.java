package com.example.axonmultientityaggregate.domain.iteration;

import com.example.axonmultientityaggregate.domain.step.Step;

public record IterationStepAddedEvent(Iteration.IterationKey key, Step step) {
}
