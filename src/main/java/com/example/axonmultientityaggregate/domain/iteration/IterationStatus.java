package com.example.axonmultientityaggregate.domain.iteration;

public enum IterationStatus {
    IN_PROGRESS, INTERRUPTED, COMPLETED, KO, PARTIAL_KO;
}
