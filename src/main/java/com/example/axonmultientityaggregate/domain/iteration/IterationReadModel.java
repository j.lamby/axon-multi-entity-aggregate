package com.example.axonmultientityaggregate.domain.iteration;

import com.example.axonmultientityaggregate.domain.step.Step;
import com.example.axonmultientityaggregate.domain.step.StepType;
import lombok.Builder;
import lombok.Value;

import java.util.Map;

@Value
@Builder
public class IterationReadModel {

    Iteration.IterationKey key;
    IterationStatus status;

    Map<StepType, Step> steps;
}
