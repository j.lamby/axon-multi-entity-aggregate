package com.example.axonmultientityaggregate.domain.iteration;

import lombok.Value;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Value
public class StartIterationCommand {

  @TargetAggregateIdentifier
  Iteration.IterationKey iterationKey;

}
