package com.example.axonmultientityaggregate.domain.substep;

public enum SubStepType {

  REMOTE_SERVICE_CALL,
  NOMINATION_PROCESSED,

}
