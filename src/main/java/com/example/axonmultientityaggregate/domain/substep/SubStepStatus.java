package com.example.axonmultientityaggregate.domain.substep;

public enum SubStepStatus {

  WAITING, OK, KO

}
