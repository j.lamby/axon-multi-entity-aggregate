package com.example.axonmultientityaggregate.domain.substep;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.EntityId;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Slf4j
@Getter
@NoArgsConstructor
public class SubStep {

    @EntityId
    private SubStepType subStepType;
    @Setter
    private SubStepStatus status = SubStepStatus.WAITING;

    public SubStep(SubStepType type) {
        this.subStepType = type;
    }

    @CommandHandler
    public void handle(EndSubStepWithStatusCommand command) {
        log.info("End substep {}", command);

        apply(new SubStepEndedWithStatusEvent(command.getKey(), command.getType(), command.getSubStepType(), command.getStatus()));
    }

    @EventSourcingHandler
    public void on(SubStepEndedWithStatusEvent event) {
        this.status = event.status();
    }

}
