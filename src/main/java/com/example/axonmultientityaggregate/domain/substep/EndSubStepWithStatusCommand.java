package com.example.axonmultientityaggregate.domain.substep;

import com.example.axonmultientityaggregate.domain.iteration.Iteration;
import com.example.axonmultientityaggregate.domain.step.StepType;
import lombok.Value;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Value
public class EndSubStepWithStatusCommand {

  @TargetAggregateIdentifier
  Iteration.IterationKey key;
  StepType type;
  SubStepType subStepType;
  SubStepStatus status;

}
