package com.example.axonmultientityaggregate.domain.substep;

import com.example.axonmultientityaggregate.domain.iteration.Iteration;
import com.example.axonmultientityaggregate.domain.step.StepType;

public record SubStepEndedWithStatusEvent(
        Iteration.IterationKey key,
        StepType type,
        SubStepType subStepType,
        SubStepStatus status) { }
