package com.example.axonmultientityaggregate.domain.step;

import com.example.axonmultientityaggregate.domain.substep.SubStepType;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public enum StepType {
  OPERATIONAL_CAPACITY_TRANSFERT_SNAPSHOT(false),
  DELETE_NOMINATIONS(true, List.of(SubStepType.REMOTE_SERVICE_CALL)),
  NOMINATION_SNAPSHOT(true),
  ACCEPTABILITY(false, List.of(SubStepType.NOMINATION_PROCESSED, SubStepType.REMOTE_SERVICE_CALL)),
  INTERNAL_MATCHING(true),
  NETWORK_CHECKS(true),
  OPTIMIZE_PHYSICAL_FLOW(false),
  EXTERNAL_MATCHING(true),
  CONFIRM_PHYSICAL_FLOW(false),
  CONFIRM_IMPLICIT_FLOW(false),
  OPTIMIZE_IMPLICIT_FLOW(false),
  BALANCE_CHECK(true),
  CONFIRMATION_NOTICE(true);

  final boolean blocking;
  final List<SubStepType> subStepTypes;

  StepType(boolean blocking) {
    this.blocking = blocking;
    this.subStepTypes = List.of();
  }
}
