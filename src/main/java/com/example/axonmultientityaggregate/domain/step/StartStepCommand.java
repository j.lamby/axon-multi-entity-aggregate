package com.example.axonmultientityaggregate.domain.step;

import com.example.axonmultientityaggregate.domain.iteration.Iteration;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

public record StartStepCommand(
        @TargetAggregateIdentifier Iteration.IterationKey key,
        StepType type,
        StepStatus status) { }
