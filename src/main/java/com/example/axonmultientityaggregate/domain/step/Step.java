package com.example.axonmultientityaggregate.domain.step;

import com.example.axonmultientityaggregate.domain.substep.SubStep;
import com.example.axonmultientityaggregate.domain.substep.SubStepType;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.modelling.command.AggregateMember;
import org.axonframework.modelling.command.EntityId;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Getter
public class Step {

    @EntityId
    private StepType type;
    private StepStatus status;

    @AggregateMember
    private Map<SubStepType, SubStep> substeps = new HashMap<>();

    public Step(StepType type, StepStatus status) {
        this.type = type;
        this.status = status;

        type.getSubStepTypes().forEach(t -> this.substeps.put(t, new SubStep(t)));
    }

}
