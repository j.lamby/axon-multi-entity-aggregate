package com.example.axonmultientityaggregate.domain.step;

public enum StepStatus {
  IN_PROGRESS, INTERRUPTED, COMPLETED, KO;
}
