package com.example.axonmultientityaggregate.domain.step;

import com.example.axonmultientityaggregate.domain.iteration.Iteration;

public record StepStartedEvent(Iteration.IterationKey key, StepType type, StepStatus status) {  }
